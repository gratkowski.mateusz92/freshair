package com.ailleron.freshair.utils

import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable


operator fun CompositeDisposable.plusAssign(disposable: Disposable) {
    add(disposable)
}

fun Disposable?.isInProgress(): Boolean {
    return !(this?.isDisposed ?: true)
}

operator fun io.reactivex.disposables.CompositeDisposable.plusAssign(disposable: io.reactivex.disposables.Disposable) {
    add(disposable)
}