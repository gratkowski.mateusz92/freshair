package com.ailleron.freshair.utils

import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import java.text.SimpleDateFormat

class FormatDate {
    companion object {
        fun formatDate(date: String): String =
            SimpleDateFormat("MMMM dd, yyyy").format(SimpleDateFormat("yyyy-MM-dd").parse(date))
    }
}