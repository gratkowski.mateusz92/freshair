package com.ailleron.freshair.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject
import javax.inject.Provider


class ViewModelProviderFactory @Inject constructor(
    private val creators: @JvmSuppressWildcards Map<Class<out ViewModel>, Provider<ViewModel>>
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        var creator: Provider<out ViewModel>? = creators[modelClass]

        if (creator == null) {
            creator = creators.entries.find { modelClass.isAssignableFrom(it.key) }?.value
        }

        if (creator == null) {
            @Suppress("ThrowRuntimeException", "TooGenericExceptionThrown")
            throw RuntimeException("Unknown view model class $modelClass")
        }

        @Suppress("UNCHECKED_CAST")
        return creator.get() as T
    }
}
