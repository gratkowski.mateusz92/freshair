package com.ailleron.freshair.di

import com.ailleron.freshair.ui.dashboard.DashboardActivity
import com.ailleron.freshair.ui.dashboard.DashboardModule
import com.ailleron.freshair.ui.splash.SplashActivity
import com.ailleron.freshair.ui.splash.SplashModule
import com.ailleron.freshair.ui.stationdetails.StationDetailsActivity
import com.ailleron.freshair.ui.stationdetails.StationDetailsModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = arrayOf(SplashModule::class))
    abstract fun splashActivity(): SplashActivity

    @ContributesAndroidInjector(modules = arrayOf(DashboardModule::class))
    abstract fun mainActivity(): DashboardActivity

    @ContributesAndroidInjector(modules = arrayOf(StationDetailsModule::class))
    abstract fun stationDetailsActivity(): StationDetailsActivity
}
