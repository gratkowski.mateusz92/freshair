package com.ailleron.freshair.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ailleron.freshair.domain.CoreRepositories
import com.ailleron.freshair.domain.models.DashboardInteractor
import com.ailleron.freshair.ui.base.BaseViewModel
import com.ailleron.freshair.ui.splash.SplashViewModel
import com.ailleron.freshair.utils.plusAssign
import com.ailleron.freshair.db.stations.Station
import com.ailleron.freshair.ui.dashboard.DashboardViewModel.DashboardViewState.SetStations
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class DashboardViewModel @Inject constructor(
    val dashboardInteractor: DashboardInteractor
) :
    BaseViewModel() {

    private val _viewState = MutableLiveData<DashboardViewState>()
    val viewState: LiveData<DashboardViewState> = _viewState

    fun init() {
        disposables += dashboardInteractor.getStations()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe { response, error ->
                handleResponse(response, error) {
                    _viewState.value = SetStations(response)
                }
            }
    }

    fun searchCity(query: String) {
        disposables += dashboardInteractor.getStationsByName(query)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe { response, error ->
                handleResponse(response, error) {
                    _viewState.value = SetStations(response)
                }
            }
    }

    sealed class DashboardViewState {
        data class SetStations(val stations : MutableList<Station>) : DashboardViewState()
    }
}
