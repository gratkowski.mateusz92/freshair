package com.ailleron.freshair.ui.stationdetails

import androidx.lifecycle.ViewModel
import com.ailleron.freshair.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class StationDetailsModule {

    @Binds
    @IntoMap
    @ViewModelKey(StationDetailsViewModel::class)
    abstract fun viewModel(viewModel: StationDetailsViewModel): ViewModel
}
