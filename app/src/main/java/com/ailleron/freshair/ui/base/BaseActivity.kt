package com.ailleron.freshair.ui.base

import android.content.DialogInterface
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import com.ailleron.freshair.R
import com.ailleron.freshair.utils.observeNotNull
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject


abstract class BaseActivity<VM : BaseViewModel> constructor(
    private val viewModelType: Class<VM>
) : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: VM
    val disposables by lazy { CompositeDisposable() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(viewModelType)
        observeError()
    }

    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }

    fun showNoInternetDialog() {
        val alertDialog: AlertDialog = AlertDialog.Builder(this).create()
        alertDialog.setMessage(getString(R.string.no_internet_connection))

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok)) { dialog, which ->
            dialog.dismiss()
        }

        alertDialog.show()
    }

    open fun showError(message: String) {
        showDefaultErrorDialog(message)
    }

    fun showDefaultErrorDialog(message: String) {
        val alertDialog: AlertDialog = AlertDialog.Builder(this).create()
        alertDialog.setTitle(getString(R.string.error))
        alertDialog.setMessage(message)

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok)) { dialog, which ->
            dialog.dismiss()
        }

        alertDialog.show()
    }

    private fun observeError() {
        viewModel.errorState.observeNotNull(this) {
            when (it) {
                is BaseViewModel.BaseViewState.OnError -> {
                    showError(it.text)
                }
                BaseViewModel.BaseViewState.NoInternet -> showNoInternetDialog()
            }
        }
    }
}
