package com.ailleron.freshair.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ailleron.freshair.domain.error.ErrorSourceInteractor
import com.ailleron.freshair.ui.base.BaseViewModel.BaseViewState.OnError
import com.ailleron.freshair.utils.NoConnectivityException
import io.reactivex.rxjava3.disposables.CompositeDisposable
import org.json.JSONObject
import retrofit2.HttpException
import javax.inject.Inject


abstract class BaseViewModel() : ViewModel() {

    private val _errorState = MutableLiveData<BaseViewState>()
    val errorState: LiveData<BaseViewState> = _errorState

    @Inject
    lateinit var errorInteractor: ErrorSourceInteractor

    protected val disposables = CompositeDisposable()

    override fun onCleared() {
        disposables.dispose()
        super.onCleared()
    }

    fun handleResponse(data: Any?, error: Throwable?, onClick: (Any) -> Unit) {
        if (error != null) {
            onError(error)
        } else {
            data?.run {
                onClick.invoke(this)
            }
        }
    }

    fun onError(error: Throwable?) {
        _errorState.value = when (error) {
            is HttpException -> {
                val errorBody = error.response()?.errorBody()

                if (errorBody != null) {
                    val jObjError = JSONObject()
                    if (jObjError.has("error"))
                        OnError(jObjError["error"].toString(), error)
                    else
                        OnError(errorInteractor.getDefaultMessage())
                } else {
                    OnError(errorInteractor.getDefaultMessage())
                }
            }
            is NoConnectivityException -> BaseViewState.NoInternet
            else -> OnError(error?.message ?: errorInteractor.getDefaultMessage(), error)
        }
    }

    sealed class BaseViewState {
        data class OnError(val text: String, val throwable: Throwable? = null) : BaseViewState()
        object NoInternet : BaseViewState()
    }
}