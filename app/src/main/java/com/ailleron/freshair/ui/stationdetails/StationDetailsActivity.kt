package com.ailleron.freshair.ui.stationdetails

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View.GONE
import com.ailleron.freshair.R
import com.ailleron.freshair.db.stations.Station
import com.ailleron.freshair.ui.base.BaseActivity
import com.ailleron.freshair.ui.stationdetails.StationDetailsViewModel.StationDetailsViewState.SetStation
import com.ailleron.freshair.ui.stationdetails.StationDetailsViewModel.StationDetailsViewState.SetStationData
import com.ailleron.freshair.utils.observeNotNull
import kotlinx.android.synthetic.main.activity_station_details.*

private const val STATION_ID = "station_id"

class StationDetailsActivity :
    BaseActivity<StationDetailsViewModel>(StationDetailsViewModel::class.java) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_station_details)

        observeViewModel()
        prepareView()
        viewModel.init(intent.getIntExtra(STATION_ID, -1))
    }

    private fun prepareView() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    private fun observeViewModel() {
        viewModel.viewState.observeNotNull(this) {
            when (it) {
                is SetStation -> setView(it.station)
                is SetStationData -> progress.setup(it.stationData)
            }
        }
    }

    private fun setView(station: Station) {
        toolbar.title = station.stationName

        city.text = getString(R.string.station_city, station.city)
        if (station.street != null) {
            street.text = getString(R.string.street, station.street)
        } else {
            street.visibility = GONE
        }
        province.text = getString(R.string.province, station.province)
    }
}

fun createStationDetailsIntent(context: Context, stationId: Int) =
    Intent(context, StationDetailsActivity::class.java).apply {
        putExtra(STATION_ID, stationId)
    }