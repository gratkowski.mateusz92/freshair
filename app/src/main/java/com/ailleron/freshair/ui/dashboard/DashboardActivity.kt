package com.ailleron.freshair.ui.dashboard

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.ailleron.freshair.R
import com.ailleron.freshair.ui.base.BaseActivity
import com.ailleron.freshair.ui.dashboard.DashboardViewModel.DashboardViewState.SetStations
import com.ailleron.freshair.ui.stationdetails.createStationDetailsIntent
import com.ailleron.freshair.utils.observeNotNull
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : BaseActivity<DashboardViewModel>(DashboardViewModel::class.java) {

    val stationsAdapter by lazy {
        StationsAdapter{
            goToStationDetailsActivity(it.idApi)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        observeViewModel()
        prepareView()
        viewModel.init()
    }

    private fun prepareView() {
        city.addTextChangedListener {
            it?.let {
                if (it.length >=3){
                    viewModel.searchCity(it.toString())
                }
            }
        }
        initAdapter()
    }


    private fun initAdapter() {
        stations.run {
            layoutManager = LinearLayoutManager(context)
            adapter = stationsAdapter
        }
    }

    private fun observeViewModel() {
        viewModel.viewState.observeNotNull(this) {
            when (it) {
                is SetStations -> stationsAdapter.setItems(it.stations)
            }
        }
    }


    private fun goToStationDetailsActivity(stationId : Int) {
        startActivity(createStationDetailsIntent(this, stationId))
    }
}

fun createDashboardIntent(context: Context) = Intent(context, DashboardActivity::class.java)