package com.ailleron.freshair.ui.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ailleron.freshair.domain.CoreRepositories
import com.ailleron.freshair.domain.models.SplashInteractor
import com.ailleron.freshair.ui.base.BaseViewModel
import com.ailleron.freshair.ui.splash.SplashViewModel.SplashViewState.*
import com.ailleron.freshair.utils.plusAssign
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    val splashInteractor : SplashInteractor
) :
    BaseViewModel() {

    private val _viewState = MutableLiveData<SplashViewState>()
    val viewState: LiveData<SplashViewState> = _viewState

    fun init() {
        disposables += splashInteractor.getStations()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe { response, error ->
                handleResponse(response, error) {
                    _viewState.value = GoToDashboard
                }
            }
    }

    sealed class SplashViewState {
        object GoToDashboard : SplashViewState()
    }
}
