package com.ailleron.freshair.ui.splash

import android.os.Bundle
import com.ailleron.freshair.R
import com.ailleron.freshair.ui.base.BaseActivity
import com.ailleron.freshair.ui.dashboard.createDashboardIntent
import com.ailleron.freshair.ui.splash.SplashViewModel.SplashViewState.GoToDashboard
import com.ailleron.freshair.utils.observeNotNull


class SplashActivity : BaseActivity<SplashViewModel>(SplashViewModel::class.java) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        viewModel.init()
        observeViewModel()
        prepareView()
    }

    private fun prepareView() {

    }

    private fun observeViewModel() {
        viewModel.viewState.observeNotNull(this) {
            when (it) {
                GoToDashboard -> goToDashboard()
            }
        }
    }

    private fun goToDashboard() {
        startActivity(createDashboardIntent(this))
    }
}