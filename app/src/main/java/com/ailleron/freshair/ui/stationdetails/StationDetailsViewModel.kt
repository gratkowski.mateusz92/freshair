package com.ailleron.freshair.ui.stationdetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ailleron.freshair.db.stations.Station
import com.ailleron.freshair.domain.models.StationDetailsInteractor
import com.ailleron.freshair.model.StationData
import com.ailleron.freshair.ui.base.BaseViewModel
import com.ailleron.freshair.ui.stationdetails.StationDetailsViewModel.StationDetailsViewState.SetStation
import com.ailleron.freshair.ui.stationdetails.StationDetailsViewModel.StationDetailsViewState.SetStationData
import com.ailleron.freshair.utils.plusAssign
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class StationDetailsViewModel @Inject constructor(
    val stationDetailsInteractor : StationDetailsInteractor
) : BaseViewModel() {

    private val _viewState = MutableLiveData<StationDetailsViewState>()
    val viewState: LiveData<StationDetailsViewState> = _viewState
    lateinit var station : Station

    fun init(stationId : Int) {
        disposables += stationDetailsInteractor.getStation(stationId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe { response, error ->
                handleResponse(response, error) {
                    station = response
                    _viewState.value = SetStation(station)
                    getIndex()
                }
            }
    }

    fun getIndex(){
        disposables += stationDetailsInteractor.getIndex(station.idApi)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe { response, error ->
                handleResponse(response, error) {
                    _viewState.value = SetStationData(response)
                }
            }
    }

    sealed class StationDetailsViewState {
        data class SetStation(val station: Station) : StationDetailsViewState()
        data class SetStationData(val stationData: StationData) : StationDetailsViewState()
    }
}
