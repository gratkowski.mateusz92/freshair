package com.ailleron.freshair.ui.dashboard

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ailleron.freshair.R
import com.ailleron.freshair.db.stations.Station
import kotlinx.android.synthetic.main.item_station.view.*

class StationsAdapter(private val onClick: (Station) -> Unit) :
    RecyclerView.Adapter<StationsAdapter.StationViewHolder>() {

    private var items = mutableListOf<Station>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StationViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_station, parent, false)
        return StationViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: StationViewHolder, position: Int) {
        holder.set(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setItems(items: MutableList<Station>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    class StationViewHolder(
        val view: View,
        val onClick: (Station) -> Unit
    ) :
        RecyclerView.ViewHolder(view) {
        fun set(station: Station) {
            view.run {
                name.text = context.getString(R.string.name, station.stationName)
                city.text = context.getString(R.string.station_city, station.city)
                setOnClickListener { onClick.invoke(station) }
            }
        }
    }
}
