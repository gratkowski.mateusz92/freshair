package com.ailleron.freshair.ui.dashboard

import androidx.lifecycle.ViewModel
import com.ailleron.freshair.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class DashboardModule {

    @Binds
    @IntoMap
    @ViewModelKey(DashboardViewModel::class)
    abstract fun viewModel(viewModel: DashboardViewModel): ViewModel
}
