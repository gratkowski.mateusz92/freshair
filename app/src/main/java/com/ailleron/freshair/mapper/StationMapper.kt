package com.ailleron.freshair.mapper

import com.ailleron.freshair.model.Station
import com.ailleron.freshair.db.stations.Station as DBStation
import javax.inject.Inject

class StationMapper @Inject internal constructor() {

    fun stationToDBStation(station: Station) = DBStation(
        idApi = station.id,
        stationName = station.name,
        lat = station.lat,
        lng = station.lng,
        city = station.city.name,
        street = station.street,
        district = station.city.commune.district,
        province = station.city.commune.province
    )

}