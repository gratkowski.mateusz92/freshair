package com.ailleron.freshair.view

import android.animation.ObjectAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.animation.DecelerateInterpolator
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.ailleron.freshair.R
import com.ailleron.freshair.model.IndexLevel
import kotlinx.android.synthetic.main.view_item_progress.view.*


class ItemProgressView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyle, defStyleRes) {
    init {
        LayoutInflater.from(context)
            .inflate(R.layout.view_item_progress, this, true)
    }

    fun setup(typeLabel: String?, indexLevel: IndexLevel?, date: String?) {
        if (typeLabel != null && indexLevel != null) {
            label.text = indexLevel.name
            label.setTextColor(ContextCompat.getColor(context, getLevelColor(indexLevel.id)))
            arcProgress.bottomText = typeLabel
            arcProgress.finishedStrokeColor =
                ContextCompat.getColor(context, getLevelColor(indexLevel.id))

            this.visibility = VISIBLE
            val animation = ObjectAnimator.ofInt(arcProgress, "progress", 0, getProgress(indexLevel.id))
            animation.duration = 1500
            animation.interpolator = DecelerateInterpolator()
            animation.start()
        } else {
            this.visibility = GONE
        }
    }

    private fun getLevelColor(levelId: Int): Int = when (levelId) {
        0 -> R.color.very_good
        1 -> R.color.good
        2 -> R.color.moderate
        3 -> R.color.sufficient
        4 -> R.color.bad
        else -> R.color.very_bad
    }

    private fun getProgress(levelId: Int): Int = when (levelId) {
        0 -> 100
        1 -> 80
        2 -> 60
        3 -> 40
        4 -> 20
        5 -> 10
        else -> 0
    }
}
