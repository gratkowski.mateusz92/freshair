package com.ailleron.freshair.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.ailleron.freshair.R
import com.ailleron.freshair.model.StationData
import kotlinx.android.synthetic.main.view_progress.view.*

class ProgressView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyle, defStyleRes) {
    init {
        LayoutInflater.from(context)
            .inflate(R.layout.view_progress, this, true)
    }

    fun setup(stationData: StationData) = stationData.run {
        st.setup("st", stIndexLevel, stCalcDate)
        so2.setup("so2", so2IndexLevel, so2CalcDate)
        no2.setup("no2", no2IndexLevel, no2CalcDate)
        pm10.setup("pm10", pm10IndexLevel, pm10CalcDate)
        pm25.setup("pm25", pm25IndexLevel, pm25CalcDate)
        o3.setup("o3", o3IndexLevel, o3CalcDate)
    }
}
