package com.ailleron.freshair.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.ailleron.freshair.db.AppDatabase.Companion.VERSION
import com.ailleron.freshair.db.stations.Station
import com.ailleron.freshair.db.stations.StationDao
import dagger.Reusable
import java.util.concurrent.Executors

@Reusable
@Database(
    entities = [
        Station::class,
    ],
    version = VERSION,
    exportSchema = false
)
@TypeConverters()
abstract class AppDatabase : RoomDatabase() {

    abstract fun stationDao(): StationDao

    companion object {
        const val DATABASE_NAME = "freshair.db"
        const val VERSION = 2
        private var instance: AppDatabase? = null
        fun getInstance(context: Context): AppDatabase = instance ?: synchronized(this) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context,
                AppDatabase::class.java,
                DATABASE_NAME
            )
                .allowMainThreadQueries()
                .build()
    }
}