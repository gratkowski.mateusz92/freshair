package com.ailleron.freshair.db.stations

import androidx.room.*

@Dao
interface StationDao {

    @Query("SELECT * FROM stations")
    fun getStations(): MutableList<Station>

    @Query("SELECT * FROM stations WHERE idApi = :stationId")
    fun getStationById(stationId : Int): Station

    @Query("SELECT * FROM stations WHERE city LIKE '%' || :query  || '%'")
    fun getStationsByName(query: String): MutableList<Station>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(station: Station): Long

    @Query("DELETE FROM stations")
    fun clear()
}