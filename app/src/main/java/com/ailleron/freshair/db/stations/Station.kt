package com.ailleron.freshair.db.stations

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "stations")
data class Station(
    @PrimaryKey(autoGenerate = true) val idStation: Int = 0,
    val idApi: Int = 0,
    val stationName: String,
    val lat: String,
    val lng: String,
    val city: String,
    val street: String?,
    val district: String,
    val province: String
)
