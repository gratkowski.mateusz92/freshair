package com.ailleron.freshair.module

import androidx.lifecycle.ViewModelProvider
import com.ailleron.freshair.di.ViewModelProviderFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun factory(factory: ViewModelProviderFactory): ViewModelProvider.Factory
}
