package com.ailleron.freshair.module

import com.ailleron.freshair.domain.remote.RemoteApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class RetrofitModule {

    @Provides
    fun remoteApi(retrofit: Retrofit): RemoteApi = retrofit.create(RemoteApi::class.java)
}