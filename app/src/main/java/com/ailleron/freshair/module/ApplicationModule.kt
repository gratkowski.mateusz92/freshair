package com.ailleron.freshair.module

import com.ailleron.freshair.BuildConfig
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
object ApplicationModule {

    @Provides
    @Named("baseUrl")
    fun baseUrl():String = BuildConfig.SERVER

    @Provides
    @Named("buildType")
    fun buildType(): String = BuildConfig.BUILD_TYPE
}
