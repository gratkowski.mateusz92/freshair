package com.ailleron.freshair.module

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(
    includes = [
        ViewModelModule::class,
        RoomModule::class,
        RetrofitModule::class,
        NetworkModule::class]
)
object AppModule {

    @JvmStatic
    @Singleton
    @Provides
    fun provideSharedPreferences(context: Context): SharedPreferences =
        context.getSharedPreferences("FreshairAssignment", Context.MODE_PRIVATE)
}