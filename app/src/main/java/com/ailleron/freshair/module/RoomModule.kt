package com.ailleron.freshair.module

import android.content.Context
import com.ailleron.freshair.db.AppDatabase
import com.ailleron.freshair.db.stations.StationDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {
    @Singleton
    @Provides
    fun provideAppDatabase(context: Context): AppDatabase = AppDatabase.getInstance(context)

    @Singleton
    @Provides
    fun provideStationDao(appDatabase: AppDatabase): StationDao = appDatabase.stationDao()
}
