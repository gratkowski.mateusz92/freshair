package com.ailleron.freshair.model

import com.google.gson.annotations.SerializedName

data class Station(
    val id: Int,
    @SerializedName("stationName") val name: String,
    @SerializedName("gegrLat") val lat: String,
    @SerializedName("gegrLon") val lng: String,
    val city: City,
    @SerializedName("addressStreet") val street: String?
)
data class City(
    val id: Int,
    val name: String,
    val commune: CityCommune
)
data class CityCommune(
    @SerializedName("communeName") val commune: String,
    @SerializedName("districtName") val district: String,
    @SerializedName("provinceName") val province: String,
)