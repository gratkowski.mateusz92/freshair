package com.ailleron.freshair.domain.models

import com.ailleron.freshair.domain.CoreRepositories
import javax.inject.Inject

class DashboardInteractor @Inject internal constructor(
    private val repositories: CoreRepositories
) {

    fun getStations() = repositories.localSource.databaseSource.stationDBInteractor.getStations()

    fun getStationsByName(query: String) =
        repositories.localSource.databaseSource.stationDBInteractor.getStationsByName(query)
}

