package com.ailleron.freshair.domain.local

import com.ailleron.freshair.domain.local.db.DatabaseSourceInteractor
import javax.inject.Inject

class LocalSourceInteractor @Inject internal constructor(
    val databaseSource: DatabaseSourceInteractor
)