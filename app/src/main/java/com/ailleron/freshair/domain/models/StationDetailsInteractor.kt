package com.ailleron.freshair.domain.models

import com.ailleron.freshair.domain.CoreRepositories
import javax.inject.Inject

class StationDetailsInteractor @Inject internal constructor(
    private val repositories: CoreRepositories
) {

    fun getStation(stationId: Int) =
        repositories.localSource.databaseSource.stationDBInteractor.getStationById(stationId)

    fun getIndex(stationId: Int) = repositories.remoteSource.getIndex(stationId)
}

