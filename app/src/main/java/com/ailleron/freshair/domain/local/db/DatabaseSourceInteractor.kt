package com.ailleron.freshair.domain.local.db

import javax.inject.Inject

class DatabaseSourceInteractor @Inject internal constructor(
    val stationDBInteractor: StationDBInteractor
)