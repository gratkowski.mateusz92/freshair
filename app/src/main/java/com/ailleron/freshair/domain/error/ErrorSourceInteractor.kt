package com.ailleron.freshair.domain.error

import android.content.Context
import com.ailleron.freshair.R
import javax.inject.Inject

class ErrorSourceInteractor @Inject constructor(private val context : Context) {

    fun getDefaultMessage():String{
        return context.getString(R.string.default_error)
    }
}