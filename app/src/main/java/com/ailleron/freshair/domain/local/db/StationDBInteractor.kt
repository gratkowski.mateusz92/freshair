package com.ailleron.freshair.domain.local.db

import com.ailleron.freshair.model.Station as AppStation
import com.ailleron.freshair.db.stations.Station
import com.ailleron.freshair.db.stations.StationDao
import com.ailleron.freshair.mapper.StationMapper
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject


class StationDBInteractor @Inject internal constructor(
    private val stationDao: StationDao,
    private val stationMapper: StationMapper
) {
    fun getStations(): Single<MutableList<Station>> = Single.just(getStationsDB())

    fun getStationsDB() = stationDao.getStations()

    fun getStationById(stationId: Int): Single<Station> = Single.just(getStationByIdDB(stationId))

    fun getStationByIdDB(stationId: Int) = stationDao.getStationById(stationId)

    fun getStationsByName(query: String): Single<MutableList<Station>> =
        Single.just(getStationsByNameDB(query))

    fun getStationsByNameDB(query: String) = stationDao.getStationsByName(query)

    fun insertStations(stations: MutableList<AppStation>) = Single.just(insertStationsDB(stations))

    fun insertStationsDB(stations: MutableList<AppStation>) {
        stationDao.clear()
        stations.forEach {
            stationDao.insert(stationMapper.stationToDBStation(it))
        }
    }
}