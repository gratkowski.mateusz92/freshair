package com.ailleron.freshair.domain

import com.ailleron.freshair.domain.local.LocalSourceInteractor
import com.ailleron.freshair.domain.remote.RemoteSourceInteractor
import dagger.Reusable
import javax.inject.Inject

@Reusable
class CoreRepositories @Inject constructor(
    val localSource: LocalSourceInteractor,
    val remoteSource: RemoteSourceInteractor
)