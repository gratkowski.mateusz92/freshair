package com.ailleron.freshair.domain.remote

import com.ailleron.freshair.model.StationData
import com.ailleron.freshair.model.Station
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface RemoteApi {
    @GET("station/findAll")
    fun getStations(): Single<MutableList<Station>>

    @GET("aqindex/getIndex/{stationId}")
    fun getIndex(@Path("stationId") stationId: Int): Single<StationData>

}