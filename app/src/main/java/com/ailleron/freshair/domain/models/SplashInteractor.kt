package com.ailleron.freshair.domain.models

import com.ailleron.freshair.domain.CoreRepositories
import javax.inject.Inject

class SplashInteractor @Inject internal constructor(
    private val repositories: CoreRepositories
) {

    fun getStations() = repositories.remoteSource.getStations()
        .flatMap {
            repositories.localSource.databaseSource.stationDBInteractor.insertStations(it)
        }
}

