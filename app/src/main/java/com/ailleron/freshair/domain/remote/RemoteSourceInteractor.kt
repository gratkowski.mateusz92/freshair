package com.ailleron.freshair.domain.remote

import javax.inject.Inject

class RemoteSourceInteractor @Inject constructor(private val remoteApi: RemoteApi) {

    fun getStations() = remoteApi.getStations()

    fun getIndex(stationId: Int) = remoteApi.getIndex(stationId)
}